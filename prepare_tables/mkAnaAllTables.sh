#!/bin/bash

source $(dirname "$0")/../configure.sh
source $(dirname "$0")/mkAnaAllTables-Config.sh

COLUMNSSINGLE='
<column title="status"/>
<column title="score" displayTitle="raw score"/>
<column title="cputime"   numberOfDigits="2" displayTitle="cpu"/>
<column title="memory"  numberOfDigits="2" displayTitle="mem"    displayUnit="MB" sourceUnit="B"/>
<column title="cpuenergy" numberOfDigits="2" displayTitle="energy" displayUnit="J"  sourceUnit="J"/>
';
COLUMNSMETA='
<column title="status"/>
<column title="score" displayTitle="raw score"/>
<column title="cputime"   numberOfDigits="2" displayTitle="cpu"/>
<column title="memory"  numberOfDigits="2" displayTitle="mem"    displayUnit="MB" sourceUnit="B"/>
<column title="cpuenergy" numberOfDigits="2" displayTitle="energy" displayUnit="J"  sourceUnit="J"/>
';

cd ${PATHPREFIX}/${RESULTSVERIFICATION}


DOCREATESINGLECATEGORY="YES";

# Single Categories
if [[ "${DOCREATESINGLECATEGORY}" == "YES" ]]; then
  for CAT in ${CATEGORIES}; do
    if [[ ! "$CAT" =~ "-" ]]; then
      continue;
    fi
    echo "Processing category $CAT";
    OUTPUT_FILE=$CAT.xml
    echo "<?xml version='1.0' ?>" > $OUTPUT_FILE
    echo "<table>${COLUMNSSINGLE}" >> $OUTPUT_FILE
    for VERIFIER in ${VERIFIERS}; do
      RESULT=$(latest_matching_filename "$VERIFIER.????-??-??_??-??-??.results.${COMPETITION}_$CAT*.xml.bz2")
      if [ -e "$RESULT" ]; then
          echo "<result filename='$RESULT'/>" >> $OUTPUT_FILE
      fi
    done
    echo "</table>" >> $OUTPUT_FILE
  done
fi

# Unioned Categories
for LINE in "${VERIFIERSLIST[@]}"; do
  METACAT=$(echo "$LINE" | cut -d ':' -f 1)
  echo "Processing meta category $METACAT";
  VERIFIERS=$(echo "$LINE" | cut -d ':' -f 2)
  OUTPUT_FILE="META_${METACAT}.xml";
  echo "<?xml version='1.0' ?>" > $OUTPUT_FILE
  echo "<table>${COLUMNSMETA}" >> $OUTPUT_FILE
  for VERIFIER in $VERIFIERS; do
    echo "    $VERIFIER";
    echo "  <union>" >> $OUTPUT_FILE
    for CATLINE in "${CATEGORIESLIST[@]}"; do
      META=$(echo "$CATLINE" | cut -d ':' -f 1)
      if [[ $META != $METACAT ]]; then
        continue;
      fi
      CATS=$(echo "$CATLINE" | cut -d ':' -f 2)
      for CAT in $CATS; do
        if [[ ! "$CAT" =~ "-" ]]; then
          continue;
        fi
        echo "        $CAT";
        RESULT=$(latest_matching_filename "$VERIFIER.????-??-??_??-??-??.results.${COMPETITION}_$CAT*.xml.bz2")
        if [ -n "$RESULT" ]; then
          echo "    <result filename='$RESULT'/>" >> $OUTPUT_FILE
        else
          echo "Result for ${VERIFIER} and ${COMPETITION}.${CAT} not found."
        fi
      done
      # Generate a table for meta category per verifier.
      OUT_META_VER=META_${METACAT}_${VERIFIER}.xml
      echo "<?xml version='1.0' ?>" > $OUT_META_VER
      echo "<table>${COLUMNSMETA}" >> $OUT_META_VER
      echo "  <union>" >> $OUT_META_VER
      for CAT in $CATS; do
        if [[ ! "$CAT" =~ "-" ]]; then
          continue;
        fi
        echo "           Meta-Category $META -- $CAT";
          FILERESULT=$(latest_matching_filename "$VERIFIER.????-??-??_??-??-??.results.${COMPETITION}_$CAT*.xml.bz2")
        if [ -e "$FILERESULT" ]; then
          echo "    <result filename='$FILERESULT'/>"  >> $OUT_META_VER;
        fi
      done
      echo "  </union>" >> $OUT_META_VER;
      echo "</table>" >> ${OUT_META_VER};
    done
    echo "  </union>" >> $OUTPUT_FILE
  done
  echo "</table>" >> $OUTPUT_FILE
done
