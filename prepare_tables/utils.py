import bz2
import fnmatch
import glob
import io
import itertools
import logging
import os
import re
import sys
import zipfile
from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from math import floor, log10
from pathlib import Path
from typing import Optional, Generator, Union
from xml.etree import ElementTree

import yaml

sys.path.append(
    str(
        Path(__file__).parent.parent.parent.resolve()
        / "fm-tools/lib-fm-tools/python/src"
    )
)
from fm_tools.fmtoolscatalog import FmToolsCatalog
from fm_tools.competition_participation import Competition, Track, JuryMember

sys.path.append(str(Path(__file__).parent.parent.parent.resolve() / "benchexec"))
from benchexec import tablegenerator

sys.path.append(str(Path(__file__).parent.parent.resolve()))

JSON_INDENT = 4

_BLACKLIST = (
    "__CLOUD__created_files.txt",
    "RunResult-*.zip",
    "*.log.data",
    "*.log.stdError",
    "fileHashes.json",
)
"""List of files that are not hashed. May include wildcard '*' and brackets '[', ']'"""


class CategoryData:
    """
    Single data measurement in category.
    """

    def __init__(
        self,
        data_total,
        data_success,
        data_success_false,
        data_unconfirmed,
        data_unconfirmed_false,
        sequence=None,
    ):
        self.total = data_total
        """ Measurement over all tasks. """

        self.success = data_success
        """ Measurement over all tasks that were solved successfully (correct + confirmed). """

        self.success_false = data_success_false
        """
            Measurement over all tasks that were solved successfully (correct + confirmed)
            and that have verdict 'false'.
        """

        self.success_true = self.success - self.success_false
        """
            Measurement over all tasks that were solved successfully (correct + confirmed)
            and that have verdict 'true'.
        """

        self.unconfirmed = data_unconfirmed
        """
            Measurement over all tasks that were solved correctly, but that were not confirmed.
        """

        self.unconfirmed_false = data_unconfirmed_false
        """
            Measurement over all tasks that were solved correctly, but that were not confirmed,
            and that have verdict 'false'.
        """

        self.unconfirmed_true = self.unconfirmed - self.unconfirmed_false
        """
            Measurement over all tasks that were solved correctly, but that were not confirmed,
            and that have verdict 'true'.
        """

        self.sequence = sequence
        """ Sequence of individual measurements. """

    def __add__(self, category_data):
        return CategoryData(
            self.total + category_data.total,
            self.success + category_data.success,
            self.success_false + category_data.success_false,
            self.unconfirmed + category_data.unconfirmed,
            self.unconfirmed_false + category_data.unconfirmed_false,
            self.sequence + category_data.sequence,
        )


class CategoryResult:
    """
    ValidationCategory result storing achieved score,
    number of true positives, true negatives, false positives and false negatives,
    as well as additional statistics.
    """

    def __init__(
        self,
        score: Decimal,
        score_false: Decimal,
        cputime: CategoryData,
        cpuenergy: CategoryData,
        correct_false: int,
        correct_true: int,
        correct_unconfirmed_false: int,
        correct_unconfirmed_true: int,
        incorrect_false: int,
        incorrect_true: int,
        qplot_cputime: list,
        qplot_cpuenergy: list,
        results_file: Optional[str],
    ):
        self.score = score
        self.score_false = score_false
        self.cputime = cputime
        self.cpuenergy = cpuenergy
        self.correct_false = correct_false
        self.correct_true = correct_true
        self.correct_unconfirmed_false = correct_unconfirmed_false
        self.correct_unconfirmed_true = correct_unconfirmed_true
        self.incorrect_false = incorrect_false
        self.incorrect_true = incorrect_true
        self.qplot_cputime = qplot_cputime
        self.qplot_cpuenergy = qplot_cpuenergy
        self.results_file = results_file

    def __add__(self, category_result):
        return CategoryResult(
            self.score + category_result.score,
            self.score_false + category_result.score_false,
            self.cputime + category_result.cputime,
            self.cpuenergy + category_result.cpuenergy,
            self.correct_false + category_result.correct_false,
            self.correct_true + category_result.correct_true,
            self.correct_unconfirmed_false + category_result.correct_unconfirmed_false,
            self.correct_unconfirmed_true + category_result.correct_unconfirmed_true,
            self.incorrect_false + category_result.incorrect_false,
            self.incorrect_true + category_result.incorrect_true,
            self.qplot_cputime + category_result.qplot_cputime,
            self.qplot_cpuenergy + category_result.qplot_cpuenergy,
            self.results_file + category_result.results_file,
        )

    def __repr__(self):
        return (
            f"CategoryResult(score={self.score}, "
            f"score_false={self.score_false}, "
            f"cputime={self.cputime}, "
            f"cpuenergy={self.cpuenergy}, "
            f"correct_false={self.correct_false}, "
            f"correct_true={self.correct_true}, "
            f"correct_unconfirmed_false={self.correct_unconfirmed_false}, "
            f"correct_unconfirmed_true={self.correct_unconfirmed_true}, "
            f"incorrect_false={self.incorrect_false}, "
            f"incorrect_true={self.incorrect_true}, "
            f"qplot_cputime={self.qplot_cputime}, "
            f"qplot_cpuenergy={self.qplot_cpuenergy}, "
            f"results_file={self.results_file})"
        )

    def __str__(self):
        return repr(self) if not self.is_empty() else "CategoryResult(empty)"

    def is_empty(self):
        return (
            self.score == 0
            and self.score_false == 0
            and self.correct_false == 0
            and self.correct_true == 0
            and self.correct_unconfirmed_false == 0
            and self.correct_unconfirmed_true == 0
            and self.incorrect_false == 0
            and self.incorrect_true == 0
            and len(self.qplot_cputime) == 0
            and len(self.qplot_cpuenergy) == 0
            and (self.results_file is None or self.results_file == "")
        )


class VerificationCategory:
    """
    VerificationCategory object storing the name of the category, the number of tasks,
    the max possible score in the category and verifiers' scores in the same.
    """

    def __init__(
        self,
        name,
        tasks=None,
        tasks_true=None,
        tasks_false=None,
        possible_score=None,
        possible_score_false=None,
    ):
        self._name = name
        self._tasks = tasks
        self._tasks_true = tasks_true
        self._tasks_false = tasks_false
        self._possible_score = possible_score
        self._possible_score_false = possible_score_false
        self.results = {}  # Participant name -> CategoryResult object

    @property
    def name(self):
        return self._name

    @property
    def tasks(self):
        return self._tasks

    @property
    def tasks_true(self):
        return self._tasks_true

    @property
    def tasks_false(self):
        return self._tasks_false

    @property
    def possible_score(self):
        return self._possible_score

    @property
    def possible_score_false(self):
        return self._possible_score_false


class ValidationCategory:
    """
    ValidationCategory object storing the name of the category, the number of tasks,
    the max possible score in the category and validators' scores in the same.
    """

    def __init__(
        self,
        name,
        tasks,
        possible_score_list,
        possible_score_false_list,
        possible_score,
        possible_score_false,
        witnesses_correct,
        witnesses_wrong,
    ):
        self._name = name
        self._tasks = tasks
        self._possible_score_list = possible_score_list
        self._possible_score_false_list = possible_score_false_list
        self._possible_score = possible_score
        self._possible_score_false = possible_score_false
        self._witnesses_correct = witnesses_correct
        self._witnesses_wrong = witnesses_wrong
        self.results = {}  # Participant name -> CategoryResult object

    @property
    def name(self):
        return self._name

    @property
    def tasks(self):
        return self._tasks

    @property
    def possible_score_list(self):
        return self._possible_score_list

    @property
    def possible_score_false_list(self):
        return self._possible_score_false_list

    @property
    def possible_score(self):
        return self._possible_score

    @property
    def possible_score_false(self):
        return self._possible_score_false

    @property
    def witnesses_correct(self):
        return self._witnesses_correct

    @property
    def witnesses_wrong(self):
        return self._witnesses_wrong

    @tasks.setter
    def tasks(self, tasks):
        self._tasks = tasks

    @possible_score_list.setter
    def possible_score_list(self, possible_score_list):
        self._possible_score_list = possible_score_list

    @possible_score_false_list.setter
    def possible_score_false_list(self, possible_score_false_list):
        self._possible_score_false_list = possible_score_false_list

    @possible_score.setter
    def possible_score(self, possible_score):
        self._possible_score = possible_score

    @possible_score_false.setter
    def possible_score_false(self, possible_score_false):
        self._possible_score_false = possible_score_false

    def __add__(self, category):
        assert self._name == category._name
        assert self.results == {} or category.results == {}
        new_category = ValidationCategory(
            self._name,
            self._tasks + category._tasks,
            self._possible_score_list + category._possible_score_list,
            self._possible_score_false_list + category._possible_score_false_list,
            self._possible_score + category._possible_score,
            self._possible_score_false + category._possible_score_false,
            self._witnesses_correct + category._witnesses_correct,
            self._witnesses_wrong + category._witnesses_wrong,
        )
        new_category.results = self.results | category.results
        return new_category


@dataclass
class TrackDetails:
    competition: Competition
    track: Track
    year: int


@dataclass
class XMLResultFileMetadata:
    validator: str  # the validator used to validate the witness
    witness: str  # the type of witness (correctness or violation)
    version: str  # the version of the witness (1.0 or 2.0)
    verifier: str  # the verifier that produced the witness
    date: str  # the date the witness was validated
    year: str  # the year of the competition
    category: str  # the category of the validation task determined by benchexec
    path: Path  # the path to the xml file

    @staticmethod
    def from_xml(xml: Union[Path, str]) -> "XMLResultFileMetadata":
        if isinstance(xml, str):
            xml = Path(xml)
        if not isinstance(xml, Path):
            raise AssertionError(
                f"Expected path or string to create metadata from file but got: {type(xml)}"
            )
        filename = xml.name
        metadata = re.match(
            r"(?P<validator>.*?)-validate-(?P<witness>.*?)-witnesses-(?P<version>[0-9\.]+)-(?P<verifier>.*?)\.(?P<date>[0-9\-\_]+)\.results\.SV-COMP(?P<year>[0-9]+)\_(?P<category>.*)\.xml\.bz2",
            (
                filename[: -len(".fixed.xml.bz2")]
                if filename.endswith(".fixed.xml.bz2")
                else filename
            ),
        )
        return XMLResultFileMetadata(
            validator=metadata.group("validator"),
            witness=metadata.group("witness"),
            version=metadata.group("version"),
            verifier=metadata.group("verifier"),
            date=metadata.group("date"),
            year=metadata.group("year"),
            category=metadata.group("category"),
            path=xml,
        )

    @staticmethod
    def from_results_validated(
        results_validated: Path,
    ) -> Generator["XMLResultFileMetadata", None, None]:
        """Find the validation data in the results_validated directory."""
        witnesses = filter(
            lambda p: not str(p).endswith(".fixed.xml.bz2"),
            results_validated.glob("*.xml.bz2"),
        )
        yield from filter(
            lambda p: "." in p.category, map(XMLResultFileMetadata.from_xml, witnesses)
        )


def is_on_blacklist(filename):
    return any(fnmatch.fnmatch(os.path.basename(filename), b) for b in _BLACKLIST)


def get_participation_labels(
    fm_tools_catalog: FmToolsCatalog,
    tool: str,
    year: int,
    competition: Competition,
    track: Track,
):
    track_list = fm_tools_catalog[tool].competition_participations.competition(
        competition, year
    )
    assert (
        len(track_list) > 0
    ), f"Tool '{tool}' does not participate in '{competition}', '{year}'."
    try:
        return track_list.labels(track)
    except KeyError as e:
        raise ValueError(
            f"Tool '{tool}' does not participate in '{competition}', track '{track}', in year '{year}'."
        ) from e


def is_hors_concours(
    fm_tools_catalog: FmToolsCatalog,
    tool: str,
    year: int,
    competition: Competition,
    track: Track,
):
    """
    Check if the given tool is hors concours in the given competition and year.
    This check only works for SV-COMP verifiers and TEST-COMP test generators.

    :param fm_tools_catalog: FmToolsCatalog object
    :param tool: tool to check
    :param year: year of the competition
    :param competition: name of the competition
    :param track: track of the competition
    :return: True if the tool is hors concours, False otherwise
    :raise ValueError: if the tool does not participate in SV-COMP or TEST-COMP in the given year
    """
    hors_concours_labels = {"inactive", "meta_tool"}
    labels = get_participation_labels(fm_tools_catalog, tool, year, competition, track)
    return len(hors_concours_labels.intersection(labels)) > 0


def get_file_number_in_zip(zipped_file) -> int:
    """Return the list of files in this zip"""
    try:
        with zipfile.ZipFile(zipped_file) as inp_zip:
            return len(
                [
                    x
                    for x in inp_zip.namelist()
                    if x.endswith(".xml") and not x.endswith("metadata.xml")
                ]
            )
    except FileNotFoundError:
        # print('Error: Zip file does not exist.')
        return 0


def round_to_sig_numbers(x: float, n: int) -> float:
    if x == 0:
        return 0
    return round(x, -int(floor(log10(abs(x)))) + (n - 1))


def parse_yaml(yaml_file):
    try:
        with open(yaml_file) as inp:
            return yaml.safe_load(inp)
    except yaml.scanner.ScannerError as e:
        logging.error("Exception while scanning %s", yaml_file)
        raise e


def write_xml_file(output_file, xml):
    if xml is None:
        logging.info("No xml for output %s", output_file)
        return
    for run in xml.findall("run"):
        # Clean-up an entry that can be inferred by table-generator automatically, avoids path confusion
        del run.attrib["logfile"]
    xml_string = (
        xml_to_string(xml).decode("utf-8").replace("    \n", "").replace("  \n", "")
    )
    if not xml_string:
        logging.info("No xml for output %s", output_file)
        return
    with io.TextIOWrapper(bz2.BZ2File(output_file, "wb"), encoding="utf-8") as xml_file:
        xml_file.write(xml_string)


def xml_to_string(
    elem,
    qualified_name="result",
    public_id="+//IDN sosy-lab.org//DTD BenchExec result 3.0//EN",
    system_id="https://www.sosy-lab.org/benchexec/result-3.0.dtd",
):
    """
    Return a pretty-printed XML string for the Element.
    Also allows setting a document type.
    """
    from xml.dom import minidom

    rough_string = ElementTree.tostring(elem, "utf-8")
    if not rough_string:
        logging.info("No xml for elem %s", elem)
        return None
    reparsed = minidom.parseString(rough_string)
    if qualified_name:
        doctype = minidom.DOMImplementation().createDocumentType(
            qualified_name, public_id, system_id
        )
        reparsed.insertBefore(doctype, reparsed.documentElement)
    return reparsed.toprettyxml(indent="  ", encoding="utf-8")


def find_latest_file_verifier(
    verifier: str,
    subcategory: str,
    competition: Competition,
    year: str,
    dir: Path,
):
    assert isinstance(year, str) and len(year) == 2, (
        "Convention demands only the last two digits of the current year "
        "in string format (leading zeros) but got: '{year}'"
    )
    prefix = f"{dir}/{verifier}"
    suffix = f"results.{competition.value}{year}_{subcategory}.xml.bz2.fixed.xml.bz2"
    path = f"{prefix}.????-??-??_??-??-??.{suffix}"
    files = glob.glob(path)
    if not files:
        return None
    # extract datetime out of filename and sort ascending by time
    files.sort(
        key=lambda name: datetime.strptime(
            str(name).replace(f"{prefix}.", "").replace(f".{suffix}", ""),
            "%Y-%m-%d_%H-%M-%S",
        )
    )
    assert len(files) > 0
    # take latest file
    latest_file = files[-1]
    return latest_file


def find_latest_file_validator(
    validator: str,
    verifier: str,
    subcategory: str,
    competition: Competition,
    year="25",
    fixed=False,
    output="results-validated",
):
    assert isinstance(year, str) and len(year) == 2, (
        "Convention demands only the last two digits of the current year "
        "in string format (leading zeros) but got: '{year}'"
    )
    assert (
        "-validate-" in validator
    ), f"Validator {validator} does not contain '-validate-'"
    processed_fixed = ".fixed.xml.bz2" if fixed else ""
    prefix = f"{output}/{validator}-{verifier}"
    suffix = f"results.{competition.value}{year}_{subcategory}.xml.bz2{processed_fixed}"
    path = f"{prefix}.????-??-??_??-??-??.{suffix}"
    files = glob.glob(path)
    if not files:
        logging.warning("No files found for %s", path)
        return None
    # extract datetime out of filename and sort by ascending by time
    files.sort(
        key=lambda name: datetime.strptime(
            str(name).replace(f"{prefix}.", "").replace(f".{suffix}", ""),
            "%Y-%m-%d_%H-%M-%S",
        )
    )
    assert len(files) > 0
    # take latest file
    latest_file = files[-1]
    return latest_file


def get_competition_tools(
    fm_tools: FmToolsCatalog,
    track_details: TrackDetails,
    include_witness_lint: bool = True,
    filter_language: Optional[set[str]] = None,
) -> list[str]:
    """
    Get all tools that participate in the given competition, track, and year.
    :@param fm_tools: Already parsed FmTools object
    :@param track_details: The competition, track, and year
    :@param include_witness_lint: If True, include witness linters in the list of tools
    :@param filter_language: If set, only include tools that support one of the given input languages
    :@return: A list of all ids of tools that participate in a given competition, track, and year
    """
    tool_query = fm_tools.query(
        track_details.competition,
        track_details.year,
        track_details.track,
    )
    tools = tool_query.verifiers()
    tools = list(sorted(tools, key=lambda v: (fm_tools.get(v).input_languages[0], v)))
    if filter_language is not None:
        tools = [
            v for v in tools if fm_tools.get(v).input_languages[0] in filter_language
        ]
    tools = tools if include_witness_lint else remove_witness_lint(tools)
    return tools


def remove_witness_lint(validators: list[str]) -> list[str]:
    """
    Remove all witness linters from the list of validators.
    :@param validators: The list of validators
    :@return: A list of validators without witness linters
    """
    return [v for v in validators if not v.startswith("witnesslint")]


def get_jury_member(
    tools: FmToolsCatalog,
    tool: str,
    track_details: TrackDetails,
) -> JuryMember:
    """
    Get the jury member for the given tool in the given year and competition_participation.
    :@param tools: Already parsed FmTools object
    :@param tool: The tool for which the jury member is requested
    :@param track_details: The details of the competition
    :@raise KeyError: if tool is not tracked in FmTools
    :@raise ValueError: if tool does not participate in any of the competitions in the year
    """
    if tool not in tools:
        raise KeyError(f"Tool {tool} is not tracked in FmTools.")
    competition_participation = tools[tool].competition_participations.competition(
        track_details.competition, track_details.year
    )
    if not competition_participation.competes_in(track_details.track):
        raise ValueError(
            f"Tool {tool} does not participate in {track_details.track} in the year {track_details.year}."
        )
    return competition_participation.tracks.get(track_details.track.value).jury_member


def round_time(value: str) -> Optional[str]:
    """
    Round a time value to two decimal places.
    """
    time_column = tablegenerator.Column(
        "Time", "", 2, "", tablegenerator.columns.ColumnMeasureType(2)
    )
    if value == "" or value is None:
        return ""
    return time_column.format_value(value, format_target="csv").strip()


def round_energy(value: str) -> Optional[str]:
    """
    Round an energy value to two decimal places.
    """
    energy_column = tablegenerator.Column(
        "Energy", "", 2, "", tablegenerator.columns.ColumnMeasureType(2)
    )
    if value == "" or value is None:
        return ""
    return energy_column.format_value(value, format_target="csv").strip()


def category_sum(vs):
    return Decimal(sum(vs))


def is_tool_status_false(status):
    return status.startswith("false")


def accumulate_data(data: list[CategoryData]) -> CategoryData:
    total = category_sum([v.total or Decimal(0) for v in data])
    success = category_sum([v.success or Decimal(0) for v in data])
    success_false = category_sum([v.success_false or Decimal(0) for v in data])
    unconfirmed = category_sum([v.unconfirmed or Decimal(0) for v in data])
    unconfirmed_false = category_sum([v.unconfirmed_false or Decimal(0) for v in data])
    return CategoryData(total, success, success_false, unconfirmed, unconfirmed_false)


def combine_qplots(qplots: list[list], category_amount) -> list:
    qplot_data = itertools.chain.from_iterable(qplots)
    return [
        ((float(score) / category_amount) if category_amount > 0 else 0, value, status)
        for score, value, status in qplot_data
    ]


def get_tool_name(tool: str, tools: FmToolsCatalog):
    try:
        return tools[tool].name
    except KeyError as e:
        logging.error("Participant not in category structure: %s", tool)
        raise e


def get_tool_url(tools: FmToolsCatalog, tool: str):
    assert tool in tools, f"Tool '{tool}' not available in FM-Tools."
    return f"https://fm-tools.sosy-lab.org/#tool-{tool}"


def get_tool_link(tools: FmToolsCatalog, tool: str):
    if tool is None:
        return "&ndash;"
    url = get_tool_url(tools, tool)
    tip = tools[tool].get("description", "")
    return f"<a href='{url}' title='{tip}'>{get_tool_name(tool, tools)}</a>"


def get_link_alltab(
    tool: str, tools: FmToolsCatalog, competition: Competition, year: int
):
    url = get_tool_url(tools, tool)
    tip = tools[tool].get("description", "")
    return f"<a href='{url}' title='{tip}'>{get_tool_name(tool, tools)}</a>"


def get_member_lines(
    tool_list: list[str], tools: FmToolsCatalog, track_details: TrackDetails
):
    result_members = "\t<tr>\n\t\t<td>Representing Jury Member</td><td></td>"
    result_affil = "\t<tr>\n\t\t<td>Affiliation</td><td></td>"
    for tool in tool_list:
        member_info = get_jury_member(tools, tool, track_details)
        assert member_info.name is not None, f"Member name missing for {tool}"
        assert (
            member_info.institution is not None
        ), f"Member institution missing for {tool}"
        assert member_info.country is not None, f"Member country missing for {tool}"
        member_name = member_info.name
        member_affiliation = f"{member_info.institution}, {member_info.country}"
        member_link = (
            f"<a href='https://orcid.org/{member_info.orcid}'>{member_name}</a>"
            if member_info.orcid != "0000-0000-0000-0000"
            else member_name
        )

        result_members += "<td>" + (member_link) + "</td>"
        result_affil += "<td>" + member_affiliation + "</td>"
    result_members += "\n\t</tr>\n"
    result_affil += "\n\t</tr>\n"
    return result_members + result_affil


def get_tool_html_and_tab(
    tools: FmToolsCatalog, tool_list: list[str], competition: Competition, year: int
):
    tool_html = (
        "\t\t<th><a href='../../systems.php'>Participants</a></th><th>Plots</th>"
    )
    tool_tab = "Participants\t\t"

    for tool in tool_list:
        logging.info(tool)
        tool_html += "<th>" + get_link_alltab(tool, tools, competition, year) + "</th>"
        tool_tab += get_tool_name(tool, tools) + "\t"

    return tool_html, tool_tab


def competition_from_string(competition: str):
    return {
        "SV-COMP": Competition.SV_COMP,
        "Test-Comp": Competition.TEST_COMP,
    }[competition]


def verifiers_of_competition(
    tools: FmToolsCatalog, competition: Competition, year: int
):
    if competition == Competition.SV_COMP:
        track = Track.Verification
    elif competition == Competition.TEST_COMP:
        track = Track.Test_Generation
    else:
        raise AssertionError(f"Competition {competition} does not exist")
    return list(sorted(tools.query(competition, year, track).verifiers()))


def validators_of_competition(
    tools: FmToolsCatalog,
    competition: Competition,
    year: int,
    include_postfix: bool = False,
) -> list[str]:
    if competition == Competition.SV_COMP:
        validation_tracks = [
            Track.Validation_Violation_1_0,
            Track.Validation_Correct_1_0,
            Track.Validation_Violation_2_0,
            Track.Validation_Correct_2_0,
        ]
    elif competition == Competition.TEST_COMP:
        validation_tracks = [
            Track.Test_Validation_Clang_Formatted,
            Track.Test_Validation_Clang_Formatted,
            Track.Test_Validation_GCC_Formatted,
            Track.Test_Validation_GCC_Unformatted,
        ]
    else:
        raise AssertionError(f"Competition {competition} does not exist")
    validators = set()
    for t in validation_tracks:
        track_validators = tools.query(competition, year, t).verifiers()
        if include_postfix:
            # convert, e.g., "Validation of Violation Witnesses 2.0" to "violation-witnesses-2.0"
            postfix = "-".join(t.value[len("Validation of ") :].lower().split())
            track_validators = [f"{v}-validate-{postfix}" for v in track_validators]
        validators.update(track_validators)
    return list(sorted(validators))


def normalize_validator_name(validator: str) -> str:
    return validator.split("-validate-", maxsplit=1)[0]
